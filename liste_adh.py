#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Script de génération de la feuille d'émargement
# Pour l'ensemble des adhérents (pour les AG)
# Auteur: ??

# Aid des membres qui n'en sont pas
exempt = [
    4281, # passoir
    6892, # doe
]

import sys
if not '/usr/scripts' in sys.path:
    sys.path.append('/usr/scripts')

import time
from lc_ldap import shortcuts
from lc_ldap import crans_utils
from gestion.config import liste_bats
ldap = shortcuts.lc_ldap_readonly()


def titre (bat) :
    if bat == 'P' :
        return 'Pavillon des Jardins'
    elif bat == '?' :
        return 'Autres (extérieurs et chambres invalides)'
    else :
        return 'Bâtiment %s' % bat.upper()

# création de la liste vide
###########################

liste = {}
for bat in liste_bats + ['?'] :
    liste[bat] = []

# remplissage de la liste
#########################
adhs = ldap.search(u'(&(finAdhesion>=%(date)s)(aid=*))' % {'date': crans_utils.to_generalized_time_format(time.time())}, sizelimit=4000)


# les extérieurs

for adh in adhs :
    bat = adh['chbre'][0].value[0].lower()
    if not bat in liste_bats :
        bat = '?'
    if not adh['aid'][0].value in exempt:
        liste[bat].append( u'%s %s' % ( adh['nom'][0].value, adh['prenom'][0].value ) )

# création du fichier tex
#########################

print """\\documentclass[a4paper,11pt]{article}
\\usepackage[T1]{fontenc}
\\usepackage[francais]{babel}
\\usepackage{longtable}
\\usepackage{geometry}
\\usepackage{fancyheadings}
\\geometry{ hmargin=2cm, vmargin=4cm }
\\usepackage[utf8]{inputenc}

\\begin{document}
\\lhead{A.G.O. du CR@NS}
\\rhead{6 Mars 2018}
\\pagestyle{fancy}
"""

for bat in liste_bats + ['?'] :
    # entête du batiment
    print """\\section{%(bat)s}\n
\\chead{\emph{%(bat)s}}
\\begin{longtable}{| p{2.5in} | p{1.5in} |}
\\hline
\\textbf{Nom Prenom} & \\textbf{Signature} \\\\
\\hline
\\endhead""" % {'bat' : titre(bat) }

    # ajout des adhérents
    liste[bat].sort()
    for adh in liste[bat] :
        print (u'%s& \\\\\n\\hline' % adh).encode('utf-8')

    # fin du bâtiment
    print "\\end{longtable}\n\\newpage\n"

print "\\end{document}"
